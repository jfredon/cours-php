<?php

namespace Lpdw\Bases;

class DeveloperConcrete extends AbstractPerson
{
    public function greeting()
    {
        return parent::greeting() . ' I\'m a developper';
    }

    public function getProfession()
    {
        return 'developer';
    }
}
