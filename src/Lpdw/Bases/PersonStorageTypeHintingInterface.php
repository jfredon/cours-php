<?php

namespace Lpdw\Bases;

interface PersonStorageTypeHintingInterface
{
    /**
     * @return PersonEncapsulated[]
     */
    public function fetchAll();

    /**
     * @param PersonEncapsulated $person
     */
    public function add(PersonEncapsulated $person);
}
