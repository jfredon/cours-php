<?php

namespace Lpdw\Bases;

interface PersonStorageInterface
{
    /**
     * @return Person[]
     */
    public function fetchAll();
}
