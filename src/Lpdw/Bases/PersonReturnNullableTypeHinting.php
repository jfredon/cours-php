<?php

namespace Lpdw\Bases;

class PersonReturnNullableTypeHinting
{
    private $firstname;
    private $lastname;

    public function getFirstname():?string
    {
        return $this->firstname;
    }

    public function setFirstname(?string $firstname)
    {
        $this->firstname = $firstname;
    }

    public function getLastname():string
    {
        return $this->lastname;
    }

    public function setLastname(string $lastname)
    {
        $this->lastname = $lastname;
    }
}
