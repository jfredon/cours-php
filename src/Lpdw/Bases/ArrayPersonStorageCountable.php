<?php

namespace Lpdw\Bases;

use Countable;

class ArrayPersonStorageCountable implements PersonStorageInterface, Countable
{
    private $storage;

    public function __construct($persons)
    {
        $this->storage = $persons;
    }

    /**
     * @return Person[]
     */
    public function fetchAll()
    {
        return $this->storage;
    }

    /**
     * @return int
     */
    public function count()
    {
        return count($this->storage);
    }
}
