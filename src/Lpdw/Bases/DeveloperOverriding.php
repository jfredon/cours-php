<?php

namespace Lpdw\Bases;

class DeveloperOverriding extends PersonEncapsulated
{
    public function greeting()
    {
        return parent::greeting() . ' I\'m a developper';
    }
}
