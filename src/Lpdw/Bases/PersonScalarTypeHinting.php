<?php

namespace Lpdw\Bases;

class PersonScalarTypeHinting
{
    private $firstname;
    private $lastname;

    public function getFirstname()
    {
        return $this->firstname;
    }

    public function setFirstname(string $firstname)
    {
        $this->firstname = $firstname;
    }

    public function getLastname()
    {
        return $this->lastname;
    }

    public function setLastname(string $lastname)
    {
        $this->lastname = $lastname;
    }
}
