<?php

namespace Lpdw\Bases;

class Manager extends Person
{
    public function greeting()
    {
        return 'Hi I\'m a manager';
    }
}
