<?php

namespace Lpdw\DesignPatterns\Factory;

abstract class Factory
{
    /**
     * @return Vehicle
     */
    abstract public function create();

    public function createWithColor(string $color):Vehicle
    {
        $vehicle = $this->create();
        $vehicle->setColor($color);
        return $vehicle;
    }
}
