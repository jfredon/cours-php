<?php

namespace Lpdw\DesignPatterns\Factory;

class CarFactory extends Factory
{
    private static $factory;

    /**
     * @before
     */
    public function setup()
    {
        self::$factory = new CarFactory();
    }

    /**
     * @return Vehicle
     */
    public function create()
    {
        return new Car();
    }
}
