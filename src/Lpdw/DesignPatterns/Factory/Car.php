<?php

namespace Lpdw\DesignPatterns\Factory;

class Car implements Vehicle
{
    private $color;

    /**
     * {@inheritdoc}
     */
    public function countWheels()
    {
        return 4;
    }

    /**
     * @param string $color
     */
    public function setColor($color)
    {
        $this->color = $color;
    }

    /**
     * @return string
     */
    public function getColor()
    {
        return $this->color;
    }
}
