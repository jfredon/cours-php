<?php

namespace Lpdw\DesignPatterns\Factory;

interface Vehicle
{
    /**
     * @return int
     */
    public function countWheels();

    /**
     * @param string $color
     */
    public function setColor($color);

    /**
     * @return string
     */
    public function getColor();
}
