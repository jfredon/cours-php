<?php

namespace Lpdw\DesignPatterns\Iterator\Collection;

use ArrayIterator;
use Iterator;

class PersonCollectionIteratorAggregate implements \IteratorAggregate
{
    private $collection;

    /**
     * PersonCollection constructor.
     * @param $collection
     */
    public function __construct(array$collection)
    {
        $this->collection = $collection;
    }

    /**
     * @return Person[]
     */
    public function getCollection():array
    {
        return $this->collection;
    }

    public function getIterator():Iterator
    {
        return new ArrayIterator($this->getCollection());
    }
}