<?php

namespace Lpdw\DesignPatterns\Iterator\Collection;

use Lpdw\DesignPatterns\Iterator\Model\Person;

class PersonCollection implements \Iterator
{
    private $currentIndex;

    private $collection;

    /**
     * PersonCollection constructor.
     * @param $collection
     */
    public function __construct(array $collection)
    {
        $this->collection = $collection;
        $this->rewind();
    }

    /**
     * @return Person
     */
    public function current()
    {
        return $this->collection[$this->currentIndex];
    }

    public function next()
    {
        $this->currentIndex++;
    }

    /**
     * @return int
     */
    public function key():int
    {
        return $this->currentIndex;
    }

    /**
     * @return bool
     */
    public function valid():bool
    {
        return array_key_exists($this->currentIndex, $this->collection);
    }

    public function rewind()
    {
        $this->currentIndex = 0;
    }
}
