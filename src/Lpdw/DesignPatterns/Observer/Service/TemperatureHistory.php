<?php

namespace Lpdw\DesignPatterns\Observer\Service;

use SplSubject;

class TemperatureHistory implements \SplObserver
{
    private $history;

    public function update(SplSubject $thermometer)
    {
        $this->history[] = $thermometer->getTemperature();
    }

    public function getHistory()
    {
        return $this->history;
    }
}
