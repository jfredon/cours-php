<?php

namespace Lpdw\DesignPatterns\Observer\Service;

use SplSubject;

class TemperatureDisplay implements \SplObserver
{
    private $ambientTemperature;

    /**
     * @param SplSubject $thermometer
     */
    public function update(SplSubject $thermometer)
    {
        $this->ambientTemperature = $thermometer->getTemperature();
    }

    /**
     * @return float
     */
    public function getAmbientTemperature():float
    {
        return $this->ambientTemperature;
    }
}
