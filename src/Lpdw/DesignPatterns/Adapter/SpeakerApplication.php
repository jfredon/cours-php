<?php

namespace Lpdw\DesignPatterns\Adapter;

class SpeakerApplication
{
    public function talk(Speech $speech)
    {
        return $speech->readText();
    }
}
