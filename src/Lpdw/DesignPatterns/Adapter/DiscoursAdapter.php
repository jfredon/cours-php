<?php

namespace Lpdw\DesignPatterns\Adapter;

class DiscoursAdapter implements Speech
{
    private $discours;

    public function __construct(Discours $discours)
    {
        $this->discours = $discours;
    }

    public function readText()
    {
        return $this->discours->lireTexte();
    }
}
