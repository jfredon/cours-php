<?php

namespace Lpdw\DesignPatterns\Adapter;

class PhpBriefing implements Briefing
{
    private $instructions;

    public function addInstruction(string $instruction)
    {
        $this->instructions[] = $instruction;
    }

    public function getAllInstructions(): string
    {
        return implode(" ", $this->instructions);
    }
}
