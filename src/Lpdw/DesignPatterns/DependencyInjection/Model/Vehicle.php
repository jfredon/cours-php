<?php

namespace Lpdw\DesignPatterns\DependencyInjection\Model;

interface Vehicle
{
    public function movingTo($address):bool;
}