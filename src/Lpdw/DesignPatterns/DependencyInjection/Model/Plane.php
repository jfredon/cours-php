<?php

namespace Lpdw\DesignPatterns\DependencyInjection\Model;

class Plane implements Vehicle
{
    public function movingTo($address):bool {
        echo 'Vous volez en direction de : ' . $address . ' - ' . date('Y-m-d H:i:s') . "\n";
        sleep(1);
        echo 'Vous êtes arrivé(e) à : ' . $address . ' - ' . date('Y-m-d H:i:s') . "\n";
        return true;
    }
}