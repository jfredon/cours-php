<?php

namespace Lpdw\DesignPatterns\DependencyInjection\Model;

class Car implements Vehicle
{
    public function movingTo($address):bool {
        echo "Vous roulez en direction de : " . $address . ' - ' . date('Y-m-d H:i:s') . "\n";
        sleep(2);
        echo "Vous êtes arrivé(e) à : " . $address . ' - ' . date('Y-m-d H:i:s') . "\n";
        return true;
    }
}