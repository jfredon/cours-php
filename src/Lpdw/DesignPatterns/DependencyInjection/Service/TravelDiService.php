<?php

namespace Lpdw\DesignPatterns\DependencyInjection\Service;

use Lpdw\DesignPatterns\DependencyInjection\Model\Vehicle;

class TravelDiService
{
    private $vehicle;

    public function __construct(Vehicle $vehicle) {
        $this->vehicle = $vehicle;
    }

    public function travelTo($destination) {
        return $this->vehicle->movingTo($destination);
    }
}