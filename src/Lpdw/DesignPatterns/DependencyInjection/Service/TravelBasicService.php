<?php

namespace Lpdw\DesignPatterns\DependencyInjection\Service;

use Lpdw\DesignPatterns\DependencyInjection\Model\PlaneBasic;

class TravelBasicService
{
    public function travelTo($destination) {
        $vehicle = new PlaneBasic(); // <1>
        return $vehicle->movingTo($destination);
    }
}