<?php

namespace Lpdw\DesignPatterns\Singleton;

class Logger
{
    private static $instance;

    public static function getInstance()
    {
        if (null === self::$instance) {
            self::$instance = new Logger();
        }

        return self::$instance;
    }

    private function __construct()
    {
    }

    public function log($level, $message)
    {
        // Code pour enregistrer le message du log.
    }
}
