<?php

namespace Lpdw\Oop;

class Manager extends AbstractPerson
{
    public function greeting()
    {
        return 'Hi I\'m a manager';
    }

    public function getProfession()
    {
        return 'Manager';
    }
}