<?php

namespace Lpdw\Oop;

abstract class AbstractPerson
{
    private $firstname;
    private $lastname;

    public function __construct($firstname, $lastname)
    {
        $this->firstname = $firstname;
        $this->lastname = $lastname;
    }

    public function getFirstname()
    {
        return $this->firstname;
    }

    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;
    }

    public function getLastname()
    {
        return $this->lastname;
    }

    public function setLastname($lastname)
    {
        $this->lastname = $lastname;
    }

    public function greeting()
    {
        return 'Hello ' . $this->firstname . '!';
    }

    public function __toString() {
        return $this->firstname . ' ' . $this->lastname;
    }

    abstract public function getProfession();
}
