<?php

namespace Lpdw\Oop;

class Developer extends AbstractPerson
{
    public function greeting()
    {
        return parent::greeting() . ' I\'m a developper';
    }

    public function getProfession()
    {
        return 'Developer';
    }
}