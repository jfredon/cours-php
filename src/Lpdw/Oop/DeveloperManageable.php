<?php

namespace Lpdw\Oop;

class DeveloperManageable extends AbstractPerson
{
    use ManageableTrait;

    public function getProfession()
    {
        return 'Developer';
    }
}