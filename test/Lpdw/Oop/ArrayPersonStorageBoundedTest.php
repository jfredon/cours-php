<?php

namespace Lpdw\Oop;

use PHPUnit\Framework\TestCase;
use Exception;

class ArrayPersonStorageBoundedTest extends TestCase
{
    /**
     * @before
     */
    public function init()
    {
        $someone = new Developer('John', 'Doe');
        $someoneElse = new Developer('Jane', 'Doe');

        $this->dummyPopulation[] = $someone;
        $this->dummyPopulation[] = $someoneElse;
    }

    /**
     * @test
     * @expectedException Exception
     */
    public function shouldNotGetDeveloper()
    {
        $personStorage = new ArrayPersonStorageBounded($this->dummyPopulation);
        $someone = $personStorage->get(2);

        $this->assertInstanceOf(AbstractPerson::class, $someone);
    }
}
