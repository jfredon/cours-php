<?php

namespace Lpdw\Oop;

use PHPUnit\Framework\TestCase;

class PersonUtilsTest extends TestCase
{
    /**
     * @test
     */
    public function shouldReturnValue()
    {
        $this->assertEquals('value', PersonUtils::staticMethod());
    }
}
