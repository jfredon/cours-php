<?php

namespace Lpdw\DesignPatterns\Observer\Service;

use Lpdw\DesignPatterns\Observer\Thermometer;
use PHPUnit\Framework\TestCase;

class TemperatureDisplayTest extends TestCase
{
    private $thermometer;
    private $temperatureDiplay;

    /**
     * @before
     */
    public function setup()
    {
        $this->thermometer  = new Thermometer();
        $this->temperatureDiplay = new TemperatureDisplay();
        $this->thermometer->attach($this->temperatureDiplay);
    }
    /**
     * @test
     */
    public function shouldBeNoyify()
    {
        $this->thermometer->setTemperature(20.5);
        $this->thermometer->notify();
        $this->assertEquals(20.5, $this->temperatureDiplay->getAmbientTemperature());

        $this->thermometer->setTemperature(22.5);
        $this->thermometer->notify();
        $this->assertEquals(22.5, $this->temperatureDiplay->getAmbientTemperature());
    }
}
