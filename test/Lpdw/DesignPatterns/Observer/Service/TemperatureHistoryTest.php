<?php

namespace Lpdw\DesignPatterns\Observer\Service;

use Lpdw\DesignPatterns\Observer\Thermometer;
use PHPUnit\Framework\TestCase;

class TemperatureHistoryTest extends TestCase
{
    /**
     * @before
     */
    public function setup()
    {
        $this->thermometer  = new Thermometer();
        $this->temperatureHistory = new TemperatureHistory();
        $this->thermometer->attach($this->temperatureHistory);
    }
    /**
     * @test
     */
    public function shouldBeNoyify()
    {
        $this->thermometer->setTemperature(20.5);
        $this->thermometer->notify();
        $this->thermometer->setTemperature(22.5);
        $this->thermometer->notify();
        $history = $this->temperatureHistory->getHistory();
        $this->assertEquals(2, count($history));
        $this->assertEquals(22.5, $history[1]);
    }
}
