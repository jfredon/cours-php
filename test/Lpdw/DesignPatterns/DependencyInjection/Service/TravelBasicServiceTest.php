<?php

namespace Lpdw\DesignPatterns\DependencyInjection\Service;

use PHPUnit\Framework\TestCase;

class TravelBasicServiceTest extends TestCase
{
    /**
     * @test
     */
    public function shouldtravelTo() {
        $travelService = new TravelBasicService();
        self::assertTrue($travelService->travelTo('Paris'));
    }
}
