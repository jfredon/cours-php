<?php

namespace Lpdw\DesignPatterns\DependencyInjection\Service;

use Lpdw\DesignPatterns\DependencyInjection\Model\Car;
use PHPUnit\Framework\TestCase;

class TravelDiServiceTest extends TestCase
{
    /**
     * @test
     */
    public function shouldtravelTo() {
        $travelService = new TravelDiService(new Car());
        self::assertTrue($travelService->travelTo('Paris'));
    }
}
