<?php

namespace Lpdw\DesignPatterns\Adapter;

use PHPUnit\Framework\TestCase;

class DiscoursAdapterTest extends TestCase
{
    /**
     * @test
     */
    public function shouldAdapteDiscours()
    {
        $speakerApplication = new SpeakerApplication();

        $discours = new PhpDiscours("Présentation de PHP");

        $discoursAdapter = new DiscoursAdapter($discours);

        $this->assertEquals("Présentation de PHP", $speakerApplication->talk($discoursAdapter));
    }

    /**
     * @test
     */
    public function shouldAdapteBriefing()
    {
        $speakerApplication = new SpeakerApplication();

        $briefing = new PhpBriefing();
        $briefing->addInstruction('instruction 1');
        $briefing->addInstruction('instruction 2');

        $briefingAdapter = new BriefingAdapter($briefing);

        $this->assertEquals("instruction 1 instruction 2", $speakerApplication->talk($briefingAdapter));
    }
}
