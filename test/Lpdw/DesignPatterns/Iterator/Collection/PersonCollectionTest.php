<?php

namespace Lpdw\DesignPatterns\Iterator\Collection;

use Lpdw\DesignPatterns\Iterator\Model\Person;
use PHPUnit\Framework\TestCase;

class PersonCollectionTest extends TestCase
{
    private $dummyPopulation;

    /**
     * @before
     */
    public function init()
    {
        $someone = new Person('John', 'Doe');
        $someoneElse = new Person('Jane', 'Doe');

        $this->dummyPopulation[] = $someone;
        $this->dummyPopulation[] = $someoneElse;
    }

    /**
     * @test
     */
    public function shouldIterateCollection()
    {
        $personCollection = new PersonCollection($this->dummyPopulation);

        self::assertTrue($personCollection->valid());
        $test = $personCollection->current();
        $this->assertEquals('John', $test->getFirstname());
        $personCollection->next();
        self::assertTrue($personCollection->valid());
        $this->assertEquals('Jane', $personCollection->current()->getFirstname());
        $personCollection->next();
        self::assertFalse($personCollection->valid());
    }

    /**
     * @test
     */
    public function shouldIterateThroughtCollection()
    {
        $personCollection = new PersonCollection($this->dummyPopulation);

        $nbPersons = 0;
        foreach ($personCollection as $person) {
            $this->assertInstanceOf(Person::class, $person);
            $nbPersons ++;
        }

        $this->assertEquals(2, $nbPersons);
    }
}
