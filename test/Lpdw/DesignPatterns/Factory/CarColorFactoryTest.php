<?php

namespace Lpdw\DesignPatterns\Factory;

use PHPUnit\Framework\TestCase;

class CarColorFactoryTest extends TestCase
{
    private static $factory;

    /**
     * @before
     */
    public function setup()
    {
        self::$factory = new CarFactory();
    }

    /**
     * @test
     */
    public function shouldCreateRedCar()
    {
        $car = self::$factory->createWithColor("red");
        $this->assertEquals(4, $car->countWheels());
        $this->assertEquals("red", $car->getColor());
    }
}
