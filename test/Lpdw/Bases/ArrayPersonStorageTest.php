<?php

namespace Lpdw\Bases;

use PHPUnit\Framework\TestCase;

class ArrayPersonStorageTest extends TestCase
{
    private $dummyPopulation;

    /**
     * @before
     */
    public function init()
    {
        $someone = new Person('John', 'Doe');
        $someoneElse = new Person('Jane', 'Doe');

        $this->dummyPopulation[] = $someone;
        $this->dummyPopulation[] = $someoneElse;
    }

    /**
     * @test
     */
    public function shouldReturnAllPersons()
    {
        $personStorage = new ArrayPersonStorage($this->dummyPopulation);
        $fetchAllResult = $personStorage->fetchAll();

        $this->assertInstanceOf(Person::class, array_shift($fetchAllResult));
        $this->assertInstanceOf(Person::class, array_shift($fetchAllResult));
        $this->assertEmpty($fetchAllResult);
    }
}
