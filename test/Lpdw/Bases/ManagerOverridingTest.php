<?php

namespace Lpdw\Bases;

use PHPUnit\Framework\TestCase;

class ManagerOverridingTest extends TestCase
{
    /**
     * @test
     */
    public function shouldReturnParentGreeting()
    {
        $someone = new Developer('John', 'Doe');

        $this->assertEquals('Hello John!', $someone->greeting());
    }

    /**
     * @test
     */
    public function shouldReturnOverrideGreeting()
    {
        $someone = new Manager('John', 'Doe');

        $this->assertEquals('Hi I\'m a manager', $someone->greeting());
    }
}
