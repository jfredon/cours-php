<?php

namespace Lpdw\Bases;

use PHPUnit\Framework\TestCase;

class PersonReturnNullableTypeHintingTest extends TestCase
{
    /**
     * @test
     * @expectedException \TypeError
     */
    public function shouldReturnErrorOnNull()
    {
        $person = new PersonReturnTypeHinting();
        $person->setFirstname(null);
        self::assertNull($person->getFirstname());
    }

    /**
     * @test
     */
    public function shouldReturnNull()
    {
        $person = new PersonReturnNullableTypeHinting();
        $person->setFirstname(null);
        self::assertNull($person->getFirstname());
    }
}
