<?php
declare(strict_types = 1);

namespace Lpdw\Bases;

use PHPUnit\Framework\TestCase;

class PersonScalarTypeHintingStrictTypeTest extends TestCase
{
    /**
     * @test
     * @expectedException TypeError
     */
    public function shouldRejectParameter()
    {
        $person = new PersonScalarTypeHinting();
        $person->setFirstname(4);
    }
}
