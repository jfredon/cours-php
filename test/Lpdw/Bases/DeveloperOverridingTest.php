<?php

namespace Lpdw\Bases;

use PHPUnit\Framework\TestCase;

class DeveloperOverridingTest extends TestCase
{
    /**
     * @test
     */
    public function shouldReturnParentGreeting()
    {
        $someone = new DeveloperOverriding('John', 'Doe');

        $this->assertEquals('Hello John! I\'m a developper', $someone->greeting());
    }

    /**
     * @test
     */
    public function shouldReturnOverrideGreeting()
    {
        $someone = new Manager('John', 'Doe');

        $this->assertEquals('Hi I\'m a manager', $someone->greeting());
    }
}
