<?php

namespace Lpdw\Bases;

use PHPUnit\Framework\TestCase;

class PersonMethodsTest extends TestCase
{
    /**
     * @test
     */
    public function shouldReturnPersonWithDifferentGreeting()
    {
        $someone = new Person('John', 'Doe');
        $someoneElse = new Person('Jane', 'Doe');

        $this->assertEquals('Hello John!', $someone->greeting());
        $this->assertEquals('Hello Jane!', $someoneElse->greeting());
    }
}
