<?php

namespace Lpdw\Bases;

use PHPUnit\Framework\TestCase;

class PersonEncapsulatedTest extends TestCase
{
    /**
     * @test
     * @expectedException Error
     */
    public function shouldReturnPersonWithDifferentFirstname()
    {
        $someone = new PersonEncapsulated('John', 'Doe');
        $someoneElse = new PersonEncapsulated('Jane', 'Doe');

        $this->assertEquals('John', $someone->firstname);
        $this->assertEquals('Jane', $someoneElse->firstname);
    }
}
