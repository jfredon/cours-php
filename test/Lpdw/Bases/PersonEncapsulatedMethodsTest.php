<?php

namespace Lpdw\Bases;

use PHPUnit\Framework\TestCase;

class PersonEncapsulatedMethodsTest extends TestCase
{
    /**
     * @test
     */
    public function shouldReturnPersonWithDifferentGreeting()
    {
        $someone = new PersonEncapsulated('John', 'Doe');
        $someoneElse = new PersonEncapsulated('Jane', 'Doe');

        $this->assertEquals('Hello John!', $someone->greeting());
        $this->assertEquals('Hello Jane!', $someoneElse->greeting());
    }

    /**
     * @test
     */
    public function shouldReturnFirstnameWithAccessor()
    {
        $someone = new PersonEncapsulated('John', 'Doe');
        $someone->setFirstname('Jane');

        $this->assertEquals('Jane', $someone->getFirstname());
    }
}
