<?php

namespace Lpdw\Bases;

use PHPUnit\Framework\TestCase;

class PersonTest extends TestCase
{
    /**
     * @test
     */
    public function shouldReturnPersonWithDifferentFirstname()
    {
        $someone = new Person('John', 'Doe');
        $someoneElse = new Person('Jane', 'Doe');

        $this->assertEquals('John', $someone->firstname);
        $this->assertEquals('Jane', $someoneElse->firstname);
    }
}
