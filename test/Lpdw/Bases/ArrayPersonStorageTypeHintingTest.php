<?php

namespace Lpdw\Bases;

use PHPUnit\Framework\TestCase;
use TypeError;

class ArrayPersonStorageTypeHintingTest extends TestCase
{
    /**
     * @test
     */
    public function shouldAddPersonEncapsulated()
    {
        $personStorage = new ArrayPersonStorageTypeHinting();
        $someone = new PersonEncapsulated('John', 'Nobody');

        $personStorage->add($someone);
        $this->assertCount(1, $personStorage);
    }

    /**
     * @test
     * @expectedException TypeError
     */
    public function shouldNotAddPerson()
    {
        $personStorage = new ArrayPersonStorageTypeHinting();
        $someone = new Person('John', 'Nobody');

        $personStorage->add($someone);
    }

    /**
     * @test
     */
    public function shouldAddDeveloper()
    {
        $personStorage = new ArrayPersonStorageTypeHinting();
        $someone = new Developer('John', 'Nobody');

        $personStorage->add($someone);
        $this->assertCount(1, $personStorage);
    }
}
