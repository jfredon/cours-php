<?php

namespace Extra\Bases;

use PHPUnit\Framework\TestCase;

class WebdesignerTest extends TestCase
{
    /**
     * @test
     */
    public function shouldReturnOverrideGreeting()
    {
        $someone = new Webdesigner('John', 'Doe');

        $this->assertEquals('Hello John Webdesigner !', $someone->greeting());
    }
}
