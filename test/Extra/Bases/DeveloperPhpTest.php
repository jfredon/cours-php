<?php

namespace Extra\Bases;

use PHPUnit\Framework\TestCase;

class DeveloperPhpTest extends TestCase
{
    /**
     * @test
     */
    public function shouldReturnOverrideGreeting()
    {
        $someone = new DeveloperPhp('John', 'Doe');

        $this->assertEquals('Hello John! I\'m a developper PHP', $someone->greeting());
    }
}
