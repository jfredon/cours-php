<?php
require_once __DIR__.'/./vendor/autoload.php';

use Lpdw\Oop\ArrayPersonStorageBounded;
use Lpdw\Oop\Developer;

$personStorage = new ArrayPersonStorageBounded(array());
$personStorage->add(new Developer('John', 'Nobody'));

try {
    $someone = $personStorage->get(1);
}
catch (Exception $e) {
    echo $e->getMessage().'<br>';
}

var_dump($someone);