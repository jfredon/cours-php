== Git

=== Workflow de contribution

Pull Requests, Code Review, and the GitHub Flow - GitHub Universe 2015
https://www.youtube.com/watch?v=vCwuZfK0VG4

The GitHub Flow - GitHub Universe 2016
https://www.youtube.com/watch?v=juLIxo42A_s

Guides GitHub
https://guides.github.com/
https://www.youtube.com/githubguides

Bases en vidéos
https://www.youtube.com/playlist?list=PLg7s6cbtAD15G8lNyoaYDuKZSKyJrgwB-

Rebase
https://www.youtube.com/watch?v=SxzjZtJwOgo&list=PLg7s6cbtAD15G8lNyoaYDuKZSKyJrgwB-&index=22

==== How-to synchronize fork project

https://help.github.com/articles/configuring-a-remote-for-a-fork/
https://help.github.com/articles/syncing-a-fork/
