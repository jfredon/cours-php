= Design patterns
:toc:
:icons: font
:numbered:
:source-highlighter: coderay
Sylvain Floury <sylvain@floury.name>
v1.1, 2017-10-10

Ce document peut être utilisé suivant les conditions de la licence http://www.gnu.org/licenses/fdl-1.3.txt[GNU Free Documentation License Version 1.3]

<<<

== Iterator

=== Principe

Ce design pattern est utilisé pour définir une façon d'accéder séquentiellement à une collection. +
Son intérêt est de *parcourir* la collection *sans connaître* son *implémentation*.

image::resources/iterator.png[]

Cette version simple de *Iterator* présente les 2 méthodes indispensables à son implémentation.

* *hasNext()* indique s'il reste des éléments dans la collection.
* *next()* retourne l'élément suivant dans la collection (s'il existe).

=== Mise en oeuvre

PHP définit une interface `Iterator` footnote:[http://php.net/manual/en/class.iterator.php] qu'il suffit d'implémenter pour mettre en oeuvre ce principe.

Son implémentation diffère quelque peu de ce que nous venons de voir.

[source, php]
----
Iterator extends Traversable {
    /* Méthodes */
    abstract public mixed current ( void )
    abstract public scalar key ( void )
    abstract public void next ( void )
    abstract public void rewind ( void )
    abstract public boolean valid ( void )
}
----

L'utilisation de ces différentes méthodes va permettre de parcourir la collection sous-jacente par leur simple utilisation.

Leur prototype nous permet de constater qu'il n'est pas nécessaire de connaître le contenu de cette collection pour les utiliser.

* current() : retourne l'élément courant.
* key() : retourne la clé de l'élément courant.
* next() : déplace l'élément courant vers l'élément suivant.
* rewind() : replace l'itérateur sur le premier élément.
* valid() : vérifie si la position courante est valide (sert à vérifier s'il reste des éléments dans la liste).

[TIP]
====
La SPL fournit un grand nombre d'implémentations dont vous voici le détail : http://php.net/manual/fr/spl.iterators.php.
====

==== PersonCollection

Nous allons créer une classe `PersonCollection` qui va implémenter le design pattern *Iterator* pour le tableau de `Person`.

Commençons par créer la classe `Person` qui constituera les éléments de cette collection.

[source,php,linenums]
.src/Lpdw/DesignPatterns/Iterator/Model/Person.php
----
include::../../src/Lpdw/DesignPatterns/Iterator/Model/Person.php[lines=5..40]
----

Créons l'implémentation de la classe `PersonCollection`.

[source,php,linenums]
.src/Lpdw/DesignPatterns/Iterator/Collection/PersonCollection.php
----
include::../../src/Lpdw/DesignPatterns/Iterator/Collection/PersonCollection.php[lines=7..55]
----

[TIP]
====
Vous noterez que nous avons utilisé le principe d'*encapsulation* sur notre collection. +
Une fois l'objet de type `PersonCollection` initialisé avec le constructeur il n'est plus possible d'accéder au tableau de `Person`.
====

Vérifiez que votre implémentation passe le test unitaire suivant.

[source,php]
----
include::../../test/Lpdw/DesignPatterns/Iterator/Collection/PersonCollectionTest.php[lines=8..40]
----

==== Solution alternative

Nous aurions pu utiliser simplement l’interface `IteratorAggregate` qui va indiquer que notre collection peut être transformer en `Iterator`.

Voici un exemple qui utilise l’une des implémentations fournies par la SPL.

[source,php,linenums]
.src/Lpdw/DesignPatterns/Iterator/Collection/PersonCollectionIteratorAggregate.php
----
include::../../src/Lpdw/DesignPatterns/Iterator/Collection/PersonCollectionIteratorAggregate.php[lines=8..33]
----

<<<

== Observer

=== Principe

Le principe est de tenir informés des objets d'un changement survenu sur un autre objet.
Les objets ainsi notifiés pourront réagir à ce changement.

L'objet surveillé est qualifié de *Sujet* (Subject) et ceux qui sont notifiés de sont changement des *Observateurs* (Observer).

image::resources/observer-diagram.png[]

L'interface `Subject` dispose de 3 méthodes .

* *attach(observer:Observer)* permet au _sujet_ d'ajouter un _observateur_.
* *detach(observer:Observer)* permet au _sujet_ de supprimer un _observateur_.
* *notify()* cette méthode déclenche la notifiaction des _observateurs_ enregistrés par le _sujet_.

Lorsque le _sujet_ souhaite notifier ses _observateurs_ (par exemple un changement d'état) il va appeler
la méthode *update(subject:Subject)* de tous ses _observateurs_.

=== Mise en application

La SPL fournit 2 interfaces pour implémenter ce principe `SplSubject` et `SplObserver`.
Nous allons nous appuyer dessus pour l'exemple suivant.

Nous allons prendre comme exemple un thermomètre dont nous souhaitons afficher la température.

Commençons par implémenter la classe de thermomètre.

image::resources/subject.png[]

Nous devons implémenter les 3 méthodes de SplSubject :

* attach : le sujet ajoute un observateur à sa liste d'objets à notifier en cas de changement.
* detach : le sujet supprime un observateur à sa liste d'objets à notifier en cas de changement.
* notify : le sujet avertit tous les observateurs de sa liste qu'un changement a eu lieu.

[source, php]
.src/Lpdw/DesignPatterns/Observer/Thermometer.php
----
include::../../src/Lpdw/DesignPatterns/Observer/Thermometer.php[lines=8..60]
----

[TIP]
====
Vous noterez l'utilisation de la classe `SplObjectStorage`. Cette classe permet de stocker facilement des collections d'objets.
Elle implémente entre autres les interfaces `Countable`, `Iterator`, `ArrayAccess`.
====

Il nous reste à implémenter un observateur.

image::resources/observer.png[]

L'interface `SplObserver` définit une seule méthode `update`.
Le sujet est passé en paramètre et nous permettra de récupérer la température.

[source, php]
.src/Lpdw/DesignPatterns/Observer/Service/TemperatureDisplay.php
----
include::../../src/Lpdw/DesignPatterns/Observer/Service/TemperatureDisplay.php[]
----

Vérifions le fonctionnement avec le test suivant.

[source, php]
.test/Lpdw/DesignPatterns/Observer/Service/TemperatureDisplay.php
----
include::../../test/Lpdw/DesignPatterns/Observer/Service/TemperatureDisplayTest.php[]
----

=== Travail à réaliser

Vous allez créer un nouvel observateur qui va stocker dans un tableau les températures à chaque fois qu'il est notifié.

* Créez la classe `TemperatureHistory` comme observateur.
* Cette classe disposera d'une méthode `getHistory()` qui retournera la liste des températures.
* Créez un nouveau fichier de tests qui vérifie le fonctionnement de cet observateur.

<<<

== Adapter

=== Principe

Le principe est de permettre à un client (service) d'utiliser une classe qu'il ne sait pas gérer.
Pour cela nous utilisons un intermédiaire qui leur permettra d'échanger.

Prenons comme exemple une application `SpeakerApplication` qui utilise un service `Speech` pour fonctionner.

Nous disposons d'un second type de service `Discours` et de son implémentation `PhpDiscours`.

Cependant `SpeakerApplication` ne sait pas utiliser ce type de service.

image::resources/adapter.png[]

Dans ce cas le design pattern *Adapter* est une solution possible.

Pour cela nous allons utiliser le principe d'_encapsulation_.

Nous créons une classe qui implémente l'interface `Speech` et dispose d'un attribut `Discours`.
Il nous reste à faire correspondre les méthodes de `Speech` et de `Discours`.

image::resources/adapter-complet.png[]


=== Mise en oeuvre

Commençons par la classe `SpeakerApplication`.

[source, php]
.src/Lpdw/DesignPatterns/Adapter/SpeakerApplication.php
----
include::../../src/Lpdw/DesignPatterns/Adapter/SpeakerApplication.php[lines=5..11]
----

Elle implémente une méthode `talk()` qui prend en paramètre une interface `Speech`.

[source, php]
.src/Lpdw/DesignPatterns/Adapter/Speech.php
----
include::../../src/Lpdw/DesignPatterns/Adapter/Speech.php[lines=5..9]
----

Considérons une autre interface pour laquelle nous disposons de classes concrètes.

[source, php]
.src/Lpdw/DesignPatterns/Adapter/Discours.php
----
include::../../src/Lpdw/DesignPatterns/Adapter/Discours.php[lines=5..18]
----

[source, php]
.src/Lpdw/DesignPatterns/Adapter/PhpDiscours.php
----
include::../../src/Lpdw/DesignPatterns/Adapter/PhpDiscours.php[lines=5..18]
----

Nous allons créer un *Adapter* pour utiliser ces classes concrètes avec la classe `Speaker`.

[source, php]
.src/Lpdw/DesignPatterns/Adapter/DiscoursAdapter.php
----
include::../../src/Lpdw/DesignPatterns/Adapter/DiscoursAdapter.php[lines=5..19]
----

Créons le test suivant pour vérifier son fonctionnement.

[source, php]
.test/Lpdw/DesignPatterns/Adapter/DiscoursAdapterTest.php
----
include::../../test/Lpdw/DesignPatterns/Adapter/DiscoursAdapterTest.php[lines=7..21]
----

=== Travail à réaliser

Soit un nouveau service `Briefing`.

[source, php]
.src/Lpdw/DesignPatterns/Adapter/Briefing.php
----
include::../../src/Lpdw/DesignPatterns/Adapter/Briefing.php[lines=5..11]
----

* Réalisez l'implémentation concrète.
* Créez un *Adapter* pour `SpeakerApplication`

<<<

== Singleton

=== Principe

Le principe est de garantir qu'une seule instance de notre classe peut-être utilisée à la fois.

Pour réaliser cela nous allons déclarer le constructeur comme *private* afin qu'il ne soit pas accessible via `new`.

Une méthode statique nous permettra de récupérer la seule instance de notre classe.

image::resources/singleton-diagram.png[]

=== Mise en application

Prenons un outil de gestion de logs comme exemple.

Pour réaliser cela nous allons déclarer le constructeur comme *private* afin qu'il ne soit pas accessible via `new`.

Nous créons ensuite une méthode statique `getInstance()` (accessible sans instancier la classe) chargée de créer l'instance si elle n'existe pas.

Bien que `private` le constructeur reste accessible aux autres méthodes de la classe. La méthode `getInstance()` peut donc l'appeler pour créer l'unique instance.

Enfin nous définissons un attribut `instance`. Il est `private` pour qu'il soit inaccessible en dehors de la classe.
Il est également `static` pour être utilisable par `getInstance()` qui est elle-même statique.

image::resources/singleton.png[]

[source, php]
.src/Lpdw/DesignPatterns/Singleton/Logger.php
----
include::../../src/Lpdw/DesignPatterns/Singleton/Logger.php[lines=5..26]
----

Vérifions le fonctionnement avec le test suivant.

[source, php]
.test/Lpdw/DesignPatterns/Singleton/LoggerTest.php
----
include::../../test/Lpdw/DesignPatterns/Singleton/LoggerTest.php[lines=7..30]
----

<<<

== Factory

=== Principe

Le design pattern `Factory` présente 2 caractéristiques.

* Il permet de créer un objet sans que le client de cette Factory n'est à gérer le processus d'instanciation.
* Il permet de masquer le processus de paramétrage de l'instance qui est retournée.

Il existe plusieurs variantes de ce pattern celui que nous allons voir la version `Factory method`.

image::resources/factory-method.png[]

Ce design pattern repose sur une version générique d'une `Fabrique` qui crée des produits génériques (`Produit`).

Des versions plus spécifiques des fabriques pourront créer des produits spécifiques (Dans cet exemple `FabriqueProduitConcret` créé `ProduitConcret`).

=== Mise en application

Prenons l'exemple suivant.

image::resources/diagram-factory-01.png[]

[source, php]
.src/Lpdw/DesignPatterns/Factory/Factory.php
----
include::../../src/Lpdw/DesignPatterns/Factory/Factory.php[lines=5..11]
----

[source, php]
.src/Lpdw/DesignPatterns/Factory/CarFactory.php
----
include::../../src/Lpdw/DesignPatterns/Factory/CarFactory.php[lines=5..14]
----

[source, php]
.src/Lpdw/DesignPatterns/Factory/Vehicle.php
----
include::../../src/Lpdw/DesignPatterns/Factory/Vehicle.php[lines=5..11]
----

[source, php]
.src/Lpdw/DesignPatterns/Factory/Car.php
----
include::../../src/Lpdw/DesignPatterns/Factory/Car.php[lines=5..14]
----

[source, php]
.test/Lpdw/DesignPatterns/Factory/CarFactoryTest.php
----
include::../../test/Lpdw/DesignPatterns/Factory/CarFactoryTest.php[lines=7..27]
----

Nous avons 2 `Factory` qui vont nous permettre de retourner des objets de type `Vehicle`.
Grâce à ce procéder nous masquons à notre test le processus d'instanciation.

* Modifiez la classe `Car` pour définir le nombre de roues depuis un constructeur.
* Modifiez la `Factory` associée.
* Lancez de nouveau le test.

Vous constatez qu'il fonctionne toujours.

Les 2 principes sont bien vérifiés :

* L'évolution du processus d'instanciation n'affecte en rien notre test.
* L'ajout de paramètres de configuration via le constructeur n'affecte pas non plus notre test.

L'utlisation de la Factory limite donc le couplage entre notre test et les instances qu'elle retourne.
Ceci est d'autant plus vrai puisque `Bicycle` et `Car` implémentent l'interface `Vehicle`.


=== Travail à réaliser

Nous souhaitons ajouter une information couleur à nos véhicules.

Modifions l'interface `Vehicle` pour ajouter la méthode adéquate.

[source, php]
.src/Vehicle.php
----
interface Vehicle
{
...
    /**
     * @param string $color
     */
    public function setColor($color);

    /**
     * @return string
     */
    public function getColor();
}
----

Soit le test suivant.

[source, php]
.tests/FactoryTest.php
----
class FactoryTest extends TestCase
{
...
    /**
     * @test
     */
    public function shouldCreateRedCar()
    {
        $car = self::$factory->createWithColor("red");
        $this->assertEquals(4, $car->countWheels());
        $this->assertEquals("red", $car->getColor());
    }
}
----

Faites les modifications suivantes pour qu'il passe.

* Modifiez la classe `Car` pour tenir compte de cette modification.
* Transformez l'interface `Factory` en classe abstraite.
* Ajoutez une méthode `createWithColor($color)` à la classe abstraite `Factory`.
* La méthode `createWithColor($color)` utilisera la méthode `setColor($color)` de la classe concrète qui l'étend.
* La méthode `createWithColor($color)` doit également initialiser la couleur du véhicule.

image::resources/diagram-factory-02.png[]

<<<

== Injection de dépendance

Commençons par voir ce qu'est la dépendance entre objets.

=== Dépendance

Si un objet fait appel explicitement à un autre objet on parle alors de dépendance.

Prenons le code suivant.

[source, php]
.src/Lpdw/DesignPatterns/DependencyInjection/Service/TravelBasicService.php
----
include::../../src/Lpdw/DesignPatterns/DependencyInjection/Service/TravelBasicService.php[]
----
<1> Dépendance du service à la classe `PlaneBasic`.

[source, php]
.src/Lpdw/DesignPatterns/DependencyInjection/Model/PlaneBasic.php
----
include::../../src/Lpdw/DesignPatterns/DependencyInjection/Model/PlaneBasic.php[]
----

Dans ce cas il y a une dépendance à la classe `PlaneBasic`. On parle ici de dépendance par composition.

Le test unitaire suivant va nous permettre de simuler une application qui l'utiliserait.

[source, php]
.test/Lpdw/DesignPatterns/DependencyInjection/Service/TravelBasicServiceTest.php
----
include::../../test/Lpdw/DesignPatterns/DependencyInjection/Service/TravelBasicServiceTest.php[]
----

=== Utilisation de l'injection de dépendance

Nous allons chercher à réduire cette dépendance de `TravelService` à `Plane`.
Ceci simplifiera l'utilisation d'autres véhicules (par exemple les voitures).

Pour cela nous utilisons une interface `Vehicle` qu'implémenteront les classes `Car` et `Plane`.

La dépendance ne sera plus fonction d'une classe mais d'une interface.
Ainsi toute classe qui implémentera cette interface pourra être utilisée.

[source, php]
.src/Lpdw/DesignPatterns/DependencyInjection/Model/Vehicle.php
----
include::../../src/Lpdw/DesignPatterns/DependencyInjection/Model/Vehicle.php[]
----

Il reste à modifier les classes `Car` et `Plane`.

[source, php]
.src/Lpdw/DesignPatterns/DependencyInjection/Model/Car.php
----
include::../../src/Lpdw/DesignPatterns/DependencyInjection/Model/Car.php[]
----

[source, php]
.src/Lpdw/DesignPatterns/DependencyInjection/Model/Plane.php
----
include::../../src/Lpdw/DesignPatterns/DependencyInjection/Model/Plane.php[]
----

Créons un nouveau service qui tiendra compte de l'injection de dépendance.

[source, php]
.src/Lpdw/DesignPatterns/DependencyInjection/Service/TravelDiService.php
----
include::../../src/Lpdw/DesignPatterns/DependencyInjection/Service/TravelDiService.php[]
----

Créons un nouveau test pour tenir compte de ces changements.

[source, php]
.test/Lpdw/DesignPatterns/DependencyInjection/Service/TravelDiServiceTest.php
----
include::../../test/Lpdw/DesignPatterns/DependencyInjection/Service/TravelDiServiceTest.php[]
----

La classe `TravelDiService` ne dépend plus de la classe `Plane`.
Le changement de véhicule s'effectue sans modifier le code de `TravelDiService`.
C'est l'application (simulée ici par le test) qui choisit l'implémentation à utiliser.


=== Dependency Injection Conainer

Il s'agit d'un outil qui met en œuvre simplement le principe d'injection de dépendance.
Son rôle est de gérer l'instanciation des services et de centraliser la configuration.

Prenons le cas d'un service d'envoi de mails. Il sera très certainement appelé à plusieurs endroits dans l'application. 
Pour gérer l'instanciation le design pattern *Factory* ou une de ces variantes est généralement utilisé.

L'utilisation d'un DIC permet de définir une seule fois l'implémentation à utiliser.
Mieux encore il est possible d'utiliser une seule instance.
Pour cela le design pattern *Singleton* est utilisé.

Pimple est un DIC relativement simple qui va nous permettre d'étudier le fonctionnement.

[NOTE]
====
*Composer* permet de l'installer avec la commande suivante.

[source, bash]
----
$composer require pimple/pimple
----
====

Créons un nouveau test pour simuler son utilisation.

[source, php]
.test/Lpdw/DesignPatterns/DependencyInjection/Service/TravelDicServiceTest.php
----
include::../../test/Lpdw/DesignPatterns/DependencyInjection/Service/TravelDicServiceTest.php[]
----

<1> Nous créons une instance du container fourni par Pimple.
<2> Nous définissons l'implémentation du véhicule à utiliser.
Dans ce cas un avion et nous demandons au container de l'enregistrer avec la clé *vehicle*.
<3> Nous définissons l'implémentation du service à utiliser.
Nous précisons sa définition(configuration) en indiquant qu'il dépendra de l'élément enregistré avec la clé *vehicle*.
Ce service sera lui-même accessible au travers de la clé *travel_service*.
<4> Nous faisons appel au service via le container.

Dans une application réelle la fonction `setupContainer()` serait sa configuration. 
Ainsi dans le reste de l'application vous n'avez plus à vous soucier de créer les instances du véhicule ou du service.
Il vous suffira d'y faire appel au travers du container.


== Si vous êtes en avance

Reprenez l'exercice sur le singleton et réalisez le travail suivant.

. Définissez 4 constantes de classe DEBUG, INFO, WARNING, ERROR qui correspondront au type de log.
Chacun des types définira un préfixe sous forme d'une chaîne de caractères.
. Ajoutez un attribut `logs` sous forme de tableau qui recevra les différents messages de logs.
. Implémentez la méthode `log($level, $message)`.
Le premier paramètre correspondra à l'un des 4 types de log. Le second paramètre sera le message à ajouter aux logs.
. Fonction du type de log le préfixe sera ajouté au début du message avant d'être ajouté à la table de logs.
. Implémentez un méthode `getLogs()` pour récupérer la liste des logs.
. Créez un test unitaire qui ajoutera un message de chaque type et vérifiera la liste obtenue.
. Ajoutez une méthode `saveLogs()` qui enregistrera les logs dans un fichier `app.logs` à la racine du projet.
. Ajoutez une méthode `setLogsPath($path)` dont le paramètre `$path` indique le chemin où enregistrer le fichier. Stockez-le dans un attribut.
. Lors de l'appel à la méthode `saveLogs()` contrôlez que le chemin du fichier est défini. Si ce n'est pas le cas levez une exception adéquate.
. Ajoutez des tests supplémentaires pour vérifier le fonctionnement si le chemin est défini ou non.

== Pour aller plus loin

* https://www.tutorialspoint.com/design_pattern/index.htm (en anglais)
* http://designpatternsphp.readthedocs.io/en/latest/README.html# (en anglais)

*Injection de dépendance*

* http://php-di.org/doc/understanding-di.html
* http://symfony.com/doc/current/service_container.html 
