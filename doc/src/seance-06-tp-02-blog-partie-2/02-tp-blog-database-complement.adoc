= Réalisation d'un blog - partie 2 - Complément

== Opérations d'écriture

Pour les opérations d'écriture nous allons nous appuyer sur les methodes proposer pour l'objet `Connection` de Doctrine DBAL.

Les méthodes d'insertion et de modification nécessitent toutes les 2 de disposer des entités sous forme de tableaux associatifs.

Nous ajoutons un second Mapping pour gérer la transformation de l'entité sous forme d'un tableau.
Nous pouvons ici parler de serialistion.

La méthode `public static function serializeEntity(Message $message):array` va gérer ceci avec un spécificité pour les `DateTime`.
Pour fonctionner avec la couche DBAL les dates doivent être sous forme de `string` dans un format reconnu par MYSQL.

Les tests unitaires des méthodes sont disponibles dans `MessageRepositoryTest`.

La méthode insert prend le nom de la table et l'entité sous forme d'un tableau associatif.
http://docs.doctrine-project.org/projects/doctrine-dbal/en/latest/reference/data-retrieval-and-manipulation.html#insert

La méthode update prend le nom de la table, l'entité sous forme d'un tableau associatif et un second tableau assoscitif qui permet d'effectuer la recherche.
http://docs.doctrine-project.org/projects/doctrine-dbal/en/latest/reference/data-retrieval-and-manipulation.html#update

La méthode delete prend uniquement un tableau assoscitif qui permet d'effectuer la recherche.
http://docs.doctrine-project.org/projects/doctrine-dbal/en/latest/reference/data-retrieval-and-manipulation.html#delete

[NOTE]
====
Les sources de la classe `Connection` présentent également les différentes méthodes.
Ceci permet de voir tous les paramètres disponibles et le fonctionnement.
====

== Discussion par rapport à la solution réalisée

Le repository ne suit pas le principe Single responsibility principle ::footnote[https://herbertograca.com/dev-theory-articles-listing/].

Les méthodes protected sont difficiles à tester.
Les méthodes protected devront être dupliquées dans tous les repositories.

Les 2 mappings pourraient être regroupés en un seul (la seule différence est le nom de la méthode set ou get).

[NOTE]
====
La déserialisation du tableau envoyé par le formulaire vers l'entité est le principe que l'on retrouve dans la gestion des formulaires de Symfony.
La partie sérialisation vers la couche DBAL sera gérée par la couche ORM de Doctrine.
====

== Mapping à l'aide du Serializer Component de Symfony

https://symfony.com/doc/current/components/serializer.html

=== Refactoring du MessageRepository

[WARNING]
====
Pour illustrer le fonctionnement du Serializer nous avons renommé l'attribut Message::date en Message::createdAt
et les getter et setter associés.
====

Nous allons utiliser la partie _normalize_ pour transformer notre entité dans le format attendu par la méthode
`Connection::insert($tableExpression, array $data, array $types = array())`.

Dans le constructeur de `MessageHydrator` nous passons à notre Serializer les 2 Normalizers que nous souhaitons utiliser.
Nous passons en second paramêtre `CamelCaseToSnakeCaseNameConverter()` à l'`ObjectNormalizer`.
C'est lui qui va assurer la transformation des noms des attributs de la classe `createdAt` en `created_at`.

Nous retrouvon dans `MessageHydrator` la méthode `hydrateRowData()` qui va transformer le résultat de la requête en objet.
Nous utilisons la méthode `denormalize()` pour passer d'un tableau à un objet (`Message`).

[NOTE]
====
Sans le petit ajout dans `hydrateRowData()` l'`ObjectNormalizer` laisse la date sous forme de string.
====

Nous retrouvon dans `MessageHydrator` la méthode `serializeEntity()` pour l'opération inverse qui va transformer un objet en tableau.
Nous utilisons la méthode `normalize()` pour passer d'un objet (`Message`) à un tableau.

=== Refactoring du controller

Nous allons créer une classe `MessageType` qui va gérer la transformation de la requête (objet `Request`) en un objet `Message`.

Nous reprennons pour cela la même signatures de méthodes que les classes qui gèrent les formulaires dans Symfony.
Ceci nous donne une version très simpliste.

les différents champs du formulaire sont dans un tableau. Nous obtenons ceci car nous avons nommé nos champs de formulaires de la façon suivante.

[source,html]
----
<input type="hidden" name="message[id]" value="{{ message.id }}">
...
<textarea class="form-control" rows="3" id="text" name="message[text]">{{ message.text }}</textarea>
----

Ils seront donc regroupés dans un tableau dont le nom sera *message*.
Nous utilisons aussi un `Serializer` avec l'`ObjectNormalizer`.

Si vous êtes en avance vous pouvez créer un nouveau Controller pour gérer les messages au format JSON.
Vous le `Serializer` en lui passant en plus le `JsonEncoder` pour gérer l'encodage et le décodage du JSON.
