= Programmation Orientée Objet
:toc:
:icons: font
:numbered:
:source-highlighter: coderay
Sylvain Floury <sylvain@floury.name>
v1.2, 2018-08-04

Ce document peut être utilisé suivant les conditions de la licence http://www.gnu.org/licenses/fdl-1.3.txt[GNU Free Documentation License Version 1.3]

<<<

== Les classes

Elles sont définies par le mot clé `class` suivi du nom de la classe suivi par des accolades.
Le nom de la classe doit avoir pour premier caractère une lettre ou un underscore. 
Les caractères suivants peuvent être des lettres, des underscores et des chiffres footnote:[http://php.net/manual/fr/language.oop5.basic.php].

Par convention le nom d'une classe est écrit en camel case footnoteref:[camelCase, https://fr.wikipedia.org/wiki/CamelCase] avec la première lettre en majuscule.

[source, php]
.src/Lpdw/Bases/EmptyPerson.php
----
include::../../../src/Lpdw/Bases/EmptyPerson.php[]
----

[NOTE]
====
Une bonne pratique est de mettre uniquement une classe par fichier.

Ce fichier porte le même nom que la classe.
====

=== Constructeur

Le mot clé `new` permet d'instancier une classe.
Si la classe définit un constructeur celui-ci est appelé. 
En PHP le constructeur est défini à l'aide de la méthode `__construct()`.

[source, php]
.test/Lpdw/Bases/EmptyPersonTest.php
----
include::../../../test/Lpdw/Bases/EmptyPersonTest.php[]
----

Dans cet exemple `$someone` est un objet de type `EmptyPerson`.

=== Les attributs

Il s'agit de variables qui sont définies au niveau de la classe et dont la valeur dépend de chaque objet (instance).

Illustrons ceci à l'aide d'un exemple.

Définissons tout d'abord la classe `Person`.

[source, php]
.src/Lpdw/Bases/Person.php
----
include::../../../src/Lpdw/Bases/Person.php[lines=1..14]
}
----

Nous allons utiliser un test unitaire pour simuler le fonctionnement attendu.

[source, php]
.test/Lpdw/Bases/PersonTest.php
----
include::../../../test/Lpdw/Bases/PersonTest.php[]
----

Lancez le test.

[source, bash]
----
$ vendor/bin/phpunit test/Lpdw/Bases/PersonTest.php
----

Ceci nous permet de vérifier que chaque objet a ses valeurs propres pour les attributs.

[NOTE]
====
Par convention le nom d'un attribut est écrit en camel case footnoteref:[camelCase] avec la première lettre en minuscule.

Il en va de même pour les variables au sens général.
====

=== Les méthodes

Les méthodes sont des fonctions attachées à la classe dont le fonctionnement va être commun à toutes les instances de cette classe.

Nous modifions la classe précédente comme suit.

[source, php]
.src/Lpdw/Bases/Person.php
----
include::../../../src/Lpdw/Bases/Person.php[]
----

La classe `Person` comporte 2 méthodes `greeting()` et le constructeur.

L'appel d'une méthode se différencie de celui d'un attribut par la présence des parenthèses.

Nous ajoutons un second test unitaire pour simuler le fonctionnement attendu.

[source, php]
.test/Lpdw/Bases/PersonMethodsTest.php
----
include::../../../test/Lpdw/Bases/PersonMethodsTest.php[]
----

Le comportement de la méthode est bien le même pour les 2 objets. Elle tient compte des attributs qui eux sont différents.

[WARNING]
====
Attention en PHP pour faire référence à un attribut de l'objet il est impératif d'utiliser `$this\->`.
Dans notre exemple `$this\->firstname`.

Il en va de même pour faire référence à une autre méthode de la classe. Par exemple `$this\->maMethode()`.
====

[NOTE]
====
Par convention le nom d'une méthode est écrit en camel case footnoteref:[camelCase] avec la première lettre en minuscule.
====

=== Visibilité

Le mot clé qui préfixe les attributs et les méthodes est appelé visibilité.
La visibilité peut prendre 3 valeurs du moins permissif au plus permissif :

* private : accessible uniquement dans la classe.
* protected : accessible dans la classe et les classes qui en héritent.
* public : accessible n'importe où.

=== Accesseurs et mutateurs

Le concept d'encapsulation sera mis en place à l'aide de ces méthodes.
L'encapsulation consiste à rendre les informations contenues dans l'objet accessibles uniquement au travers de ces méthodes.

L'idée est de s'assurer des informations contenues et même de permettre une validation lors de leur ajout.

Modifions la classe `Person` pour que les attributs soient inaccessibles en dehors de la classe.

[source, php]
.src/Lpdw/Bases/PersonEncapsulated.php
----
include::../../../src/Lpdw/Bases/PersonEncapsulated.php[]
----

Il nous faut adapter les tests pour qu'ils passent.

[source, php]
.test/Lpdw/Bases/PersonEncapsulatedTest.php
----
include::../../../test/Lpdw/Bases/PersonEncapsulatedTest.php[]
----

[WARNING]
====
Ce test ne fonctionne qu'à partir de PHP7.
====

[source, php]
.test/Lpdw/Bases/PersonEncapsulatedMethodsTest.php
----
include::../../../test/Lpdw/Bases/PersonEncapsulatedMethodsTest.php[]
----

<<<

=== Héritage

L'héritage en PHP est obtenu à l'aide du mot clé `extends`.
Il permet d'hériter des attributs et des méthodes d'une autre classe.
Cette classe est appelée classe *parente*.

Pour qu'une méthode puisse être héritée sa visibilité devra être `protected` ou `public`.

Il est important de noter que PHP permet d'hériter *d'une seule classe*.

Ajoutons une nouvelle classe `Developer` qui hérite de `Person`.

[source, php]
.src/Lpdw/Bases/Developer.php
----
include::../../../src/Lpdw/Bases/Developer.php[]
----

Le test suivant permet de vérifier ces notions d'héritage.

[source, php]
.test/Lpdw/Bases/DeveloperTest.php
----
include::../../../test/Lpdw/Bases/DeveloperTest.php[]
----

Vous constatez que grâce à l'héritage la classe `Developer` permet l'utilisation de l'accesseur `getFirstname()`.


==== Redéfinition

La redéfinition (ou spécialisation, en anglais overriding) est une forme de *polymorphisme* liée à l'héritage.

Ceci consiste à redéfinir une méthode de la classe parente.

Créons une nouvelle classe `Manager` où nous allons redéfinir la méthode `greeting()`.

[source, php]
.src/Lpdw/Bases/Manager.php
----
include::../../../src/Lpdw/Bases/Manager.php[]
----

Effectuons le test suivant.

[source, php]
.test/Lpdw/Bases/ManagerOverridingTest.php
----
include::../../../test/Lpdw/Bases/ManagerOverridingTest.php[]
----

La classe `Manager` ne tient plus compte de la méthode héritée.


==== Classe parent

Dans le cas d'une redéfinition de méthode il est parfois utile de pouvoir accéder à la méthode redéfinie (celle de la classe parente).

Redéfinissons la méthode `greeting()` de la classe `Developer`.

[source, php]
.src/Lpdw/Bases/DeveloperOverriding.php
----
include::../../../src/Lpdw/Bases/DeveloperOverriding.php[]
----

Pour faire référence à la méthode de la classe parente son nom doit être préfixé de `parent::`.

Utilisons le test suivant pour étudier ce changement.

[source, php]
.test/Lpdw/Bases/DeveloperOverridingTest.php
----
include::../../../test/Lpdw/Bases/DeveloperOverridingTest.php[]
----

Cette fois nous tenons compte de la méthode héritée. Pour cela nous avons du faire un appel explicite.

<<<

== Classe abstraite

Une classe abstraite est une classe qui dispose d'attributs et de méthodes comme n'importe quelle classe mais dont au moins l'une de ses méthodes est *abstraite*.

Cela signifie que le prototype de la fonction est défini mais que la méthode n'a pas de corps.

Le mot clé `abstract` est utilisé aussi bien pour les classes que pour les méthodes.

Les classes abstraites ne peuvent pas être instanciées et doivent par conséquent être dérivées par des classes *concrètes*.

Créons une nouvelle classe abstraite `AbstractPerson` dans laquelle nous allons reporter tout le code présent dans la classe `Person`. +
Nous ajoutons une méthode abstraite `getProfession()` qu'implémenteront chaque sous classe.

[source, php]
.src/Lpdw/Bases/AbstractPerson.php
----
include::../../../src/Lpdw/Bases/AbstractPerson.php[lines=1..10]
...
include::../../../src/Lpdw/Bases/AbstractPerson.php[lines=36..43]
----

Soit la classe `DeveloperConcrete` une implémentation concrète qui en hérite.

[source, php]
.src/Lpdw/Bases/DeveloperConcrete.php
----
include::../../../src/Lpdw/Bases/DeveloperConcrete.php[]
----

Vérifions le fonctionnement avec le test suivant.

[source, php]
.test/Lpdw/Bases/DeveloperConcreteTest.php
----
include::../../../test/Lpdw/Bases/DeveloperConcreteTest.php[]
----

====
*À réaliser*

* Réalisez une nouvelle implémentation concrète de la classe `AbstractPerson` qui portera le nom de `Webdesigner`.
* Pensez à créer un test unitaire pour valider son fonctionnement.
====

<<<

== Interfaces

Les interfaces permettent de spécifier les prototypes de méthodes que devront utiliser les classes qui souhaitent les implémenter.

Une interface est une forme de *contrat*.

Contrairement à l'héritage une classe peut implémenter plusieurs interfaces.

Les interfaces sont déclarées à l'aide du mot clé `interface`.

Le mot clé `implements` définit qu'une classe *implémente* une interface.

[WARNING]
====
Une interface ne peut déclarer que des méthodes publiques.
====

Nous allons créer un service qui gère une liste de personnes. 

. Créons d'abord une interface qui servira de contrat pour l'utilisation de notre service.
. Puis le service lui-même qui implémentera cette interface.


Créons une interface `PersonStorageInterface` qui définit les méthodes du service de stockage.

[source, php]
.src/Lpdw/Bases/PersonStorageInterface.php
----
include::../../../src/Lpdw/Bases/PersonStorageInterface.php[]
----

Créons maintenant une implémentation concrète de cette interface qui repose sur un tableau.

[source, php]
.src/Lpdw/Bases/ArrayPersonStorage.php
----
include::../../../src/Lpdw/Bases/ArrayPersonStorage.php[]
----

Vérifions le fonctionnement avec le test suivant.

[source, php]
.test/Lpdw/Bases/ArrayPersonStorageTest.php
----
include::../../../test/Lpdw/Bases/ArrayPersonStorageTest.php[]
----

[NOTE]
====
Nous pourrions créer une implémentation de cette interface qui repose sur une base de données au lieu d'un tableau. +
Si ces 2 services implémentent la même interface alors ils seront interchangeables. +
Nous reviendrons sur ce point lorsque nous aborderons l'injection de dépendances.
====

Pour implémenter une nouvelle interface il suffit de l'ajouter à la suite de la précédente en les séparant par une virgule.

À titre d'exemple ajoutons l'interface de la SPL `Countable`.

Cette interface requiert l'implémentation d'une méthode `count()` qui retourne le nombre d'éléments de la liste.

[source, php]
.src/Lpdw/Bases/ArrayPersonStorageCountable.php
----
include::../../../src/Lpdw/Bases/ArrayPersonStorageCountable.php[]
----

Ajoutons un test pour vérifier le fonctionnement.

[source, php]
.test/Lpdw/Bases/ArrayPersonStorageCountableTest.php
----
include::../../../test/Lpdw/Bases/ArrayPersonStorageCountableTest.php[]
----

<<<

== Type hinting

Le typage objet (type hinting) permet de préciser un type pour les paramètres.

L'interface utilise le typage dans la méthode `add(PersonEncapsulated $person)`.
Cette méthode indique que le paramètre doit être du type `PersonEncapsulated`.

[source, php]
.src/Lpdw/Bases/PersonStorageTypeHintingInterface.php
----
include::../../../src/Lpdw/Bases/PersonStorageTypeHintingInterface.php[]
----

La classe `ArrayPersonStorageTypeHinting` implémente cette interface.

[source, php]
.src/Lpdw/Bases/ArrayPersonStorageTypeHinting.php
----
include::../../../src/Lpdw/Bases/ArrayPersonStorageTypeHinting.php[]
----

Le test suivant illustre le fonctionnement.

[WARNING]
====
Ce test ne fonctionne qu'à partir de PHP7.
====

[source, php]
.test/Lpdw/Bases/ArrayPersonStorageTypeHintingTest.php
----
include::../../../test/Lpdw/Bases/ArrayPersonStorageTypeHintingTest.php[]
----

Le test de la méthode `add(PersonEncapsulated $person)` avec une instance de `PersonEncapsulated` passe évidement.

Le test avec une instance de `Person` lève lui une erreur spécifique `TypeError`.

Enfin grâce au polymorphisme le test avec une instance de `Developer` fonctionne car `Developer` sous-classe `PersonEncapsulated`.


[NOTE]
====
Le type hinting dépend de la version de PHP utilisée. 
L'exemple précédent fonctionne sous toutes les versions que vous serez amené à rencontrer (PHP > 5.0).

Pour plus de précisions sur ce point consultez : http://php.net/manual/fr/functions.arguments.php#functions.arguments.type-declaration.types.
====

=== Aller plus loin en PHP 7

==== Scalar type hinting

PHP 7 permet d'utiliser le type hinting même pour les types scalaires c'est à dire :

* booléen : `bool`
* réel : `float`
* entier : `int`
* chaine de caractères : `string`

Nous ajoutons une nouvelle classe `PersonScalarTypeHinting` qui utilise le type `string`.

[source, php]
.src/Lpdw/Bases/PersonScalarTypeHinting.php
----
include::../../../src/Lpdw/Bases/PersonScalarTypeHinting.php[]
----

Le test suivant permet d'effectuer le  test avec un entier à la place d'une chaine de caractères.

[source, php]
.test/Lpdw/Bases/PersonScalarTypeHintingTest.php
----
include::../../../test/Lpdw/Bases/PersonScalarTypeHintingTest.php[]
----

Nous voyons que même si nous avons précisé le type string le test passe quand même.

PHP 7 propose 2 modes pour le controle des types le mode *strict* et le mode *weak*.
Le mode *weak* va transformer l'entier en chaine de caractère et fonctionner.
Le mode *strict* va lui lever une erreur `TypeError`.

Comme le laisse deviner cette exemple le mode *weak* est le mode par défaut.

Le test suivant effectue utilise le mode *strict*.

[source, php]
.test/Lpdw/Bases/PersonScalarTypeHintingStrictTypeTest.php
----
include::../../../test/Lpdw/Bases/PersonScalarTypeHintingStrictTypeTest.php[]
----

==== Type hinting de la valeur de retour

Depuis PHP 7 il est égalament possible de préciser le type de retour d'une méthode.

Le type de retour est précisé en fin de signature de la méthode précédé de "*:*".

[source, php]
.src/Lpdw/Bases/PersonReturnTypeHinting.php
----
include::../../../src/Lpdw/Bases/PersonReturnTypeHinting.php[]
----

Le fonctionnement du type hinting de la valeur de retour est affecté de la même façon par le mode de contrôle *strict* ou *weak*.

==== Nullable types

Cette fonctionnalité est disponble uniquement depuis PHP 7.1 et permet de préciser que le type peut être `null`.

C'est à dire que le paramètre ou la valeur de retour pourront être soit du type choisi (i.e. : DateTime) soit `null`.

Si nous effectuons le test suivant nous obtenons une erreur `TypeError`.

[source, php]
.test/Lpdw/Bases/PersonReturnNullableTypeHintingTest.php
----
include::../../../test/Lpdw/Bases/PersonReturnNullableTypeHintingTest.php[lines=9..18]
----

La classe PersonReturnNullableTypeHinting gère un type nullable.
Pour cela le type est préfixé d'un *?*.

[source, php]
.src/Lpdw/Bases/PersonReturnNullableTypeHinting.php
----
include::../../../src/Lpdw/Bases/PersonReturnNullableTypeHinting.php[]
----

Le test suivant permet de vérifier le fonctionnement attendu.

[source, php]
.test/Lpdw/Bases/PersonReturnNullableTypeHintingTest.php
----
include::../../../test/Lpdw/Bases/PersonReturnNullableTypeHintingTest.php[lines=20..28]
----

<<<

== Travail à réaliser

* Faites en sorte que `Manager` et `Developer` héritent de `AbstractPerson`.
* Modifiez `PersonStorageTypeHintingInterface` et `ArrayPersonStorageTypeHinting` pour que les méthodes utilisent `AbstractPerson` au lieu de `PersonEncapsulated`.
* Modifiez les tests unitaires de façon à ce que la population soit composée d'un `Manager` et d'un `Developer`.
* Ajoutez une méthode `findByFirstname($firstname)` à l'interface `PersonStorageTypeHintingInterface`.
* Implémentez la méthode `findByFirstname($firstname)` de manière à ce qu'elle retourne un tableau des personnes qui portent ce prénom.

== Si vous êtes en avance

Nous souhaitons réaliser une application de gestion de notes.

Nous partons pour ce la des hypothèses suivantes.

Les étudiants auront les informations suivantes :
* nom,
* prénom,
* identifiant (composé de la première lettre du prénom suivi des 2 premières lettres du nom)

Les évaluations auront les informations suivantes :

* note (entre 0 et 20),
* coéfficient (entre 1 et 3)

L'application permettra les opérations suivantes :

* Ajouter un étudiant,
* Ajouter une évaluation à un étudiant,
* Faire la moyenne de l'étudiant.

Simulez l'application à l'aide de tests unitaires.

== Pour aller plus loin

* http://php.net/language.oop5
* https://openclassrooms.com/courses/programmez-en-oriente-objet-en-php
* https://knpuniversity.com/screencast/oo (en anglais, version complète payante).
