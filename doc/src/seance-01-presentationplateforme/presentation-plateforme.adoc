= Présentation de la plateforme
:toc:
:icons: font
:numbered:
:source-highlighter: coderay
Sylvain Floury <sylvain@floury.name>
v1.1, 2017-09-30

Ce document peut être utilisé suivant les conditions de la licence http://www.gnu.org/licenses/fdl-1.3.txt[GNU Free Documentation License Version 1.3]

<<<

== Git

Nous utiliserons la plateforme Git mise à disposition par l'université au cours de cet enseignement.

Commençons par créer une clé RSA que nous utiliserons pour simplifier l'utilisation de Git.

. lancez le terminal
. depuis le terminal lancez la commande `ssh-keygen`.
. laissez le chemin proposé.
. choisissez une passphrase.

[source, bash]
----
$ ssh-keygen 
Generating public/private rsa key pair.
Enter file in which to save the key (/home/lpdw/.ssh/id_rsa): 
Enter passphrase (empty for no passphrase): 
Enter same passphrase again: 
key_save_private: passphrase is too short (minimum four characters)
Your identification has been saved in /home/lpdw/.ssh/id_rsa.
Your public key has been saved in /home/lpdw/.ssh/id_rsa.pub.
The key fingerprint is:
8c:22:d5:4f:f3:b0:61:d1:7c:08:ba:f9:01:7e:64:bb iut@debian-LPDW
The key's randomart image is:
+---[RSA 2048]----+
|        o+ .     |
|     . . .+ .    |
|    . + O  .     |
|   . . % B       |
|  . . = S .      |
|   . . o o       |
|        E        |
|                 |
|                 |
+-----------------+
----

. Connectez-vous maintenant au GitLab de l'université footnote:[https://git.unilim.fr] en utilisant votre login/password de l'université.
. Allez dans la section `Settigns > SSH Keys`.
. Copiez le contenu du fichier /home/lpdw/.ssh/id_rsa.pub
. Ajoutez la clé à votre compte.

Nous allons tester le bon fonctionnement de votre configuration.

Créez un dossier `test` dans le dossier `/home/lpdw`. Lancez ensuite la commande suivante.

[source, bash]
----
iut@debian-LPDW:~$ git clone git@git.unilim.fr:flourv01/test.git
Clonage dans 'test'...
Enter passphrase for key '/home/lpdw/.ssh/id_rsa': 
remote: Décompte des objets: 3, fait.
remote: Total 3 (delta 0), reused 0 (delta 0)
Réception d'objets: 100% (3/3), fait.
Vérification de la connectivité... fait.
----

<<<

== Environnement PHP

La distribution de PHP fournie par Fedora propose 2 versions :

- la version CGI qui va interagir avec un serveur Web comme apache.
- la version CLI accessible en ligne de commande.

Nous utiliserons principalement de la CLI PHP footnote:[http://php.net/manual/fr/features.commandline.php].

Elle permettre d'exécuter la commande `php` directement depuis le terminal.

Lancez la commande suivante pour vérifier la version de PHP utilisée.

[source, bash]
----
$ php -v
----

Pour illustrer ces 2 modes de fonctionnement commençons par créer le fichier suivant.

[source, php]
.index.php
----
<?php

echo "Hello world \n";
----

=== Utilisation avec Apache

Placez ce fichier dans le dossier `/var/www/html`.

Ouvrez un navigateur et saisissez l'URL `http://localhost/`.

Vous devriez voir apparaitre le message "Hello world" dans le navigateur.

[WARNING]
====
Vérifiez que le serveur Apache est démarré.

Pour cela connectez-vous en root, lancez la commande suivante et vérifiez qu'il soit actif.

[source, bash]
----
# systemctl status httpd
● httpd.service - The Apache HTTP Server
   Loaded: loaded (/usr/lib/systemd/system/httpd.service; disabled; vendor preset: disabled)
   Active: active (running) since Mon 2017-10-02 19:15:39 CEST; 2s ago

   ...
----

Si ce n'est pas le cas lancez-le à l'aide de la commande suivante.

[source, bash]
----
# systemctl start httpd
----

====

=== Utilisation de la CLI

Toujours à partir du même dossier `/var/www/html` exécutez la commande suivante depuis le terminal.

[source, bash]
----
$ php index.php
----

Vous devriez voir apparaitre le message "Hello world" dans le terminal.

=== Serveur web interne

La CLI intègre un serveur web interne qui s'utilise de la façon suivante.

Toujours à partir du même dossier `/var/www/html` exécutez la commande suivante depuis le terminal.

[source, bash]
----
$ php -S localhost:8000 index.php
----

Ouvrez un navigateur et saisissez l'URL `http://localhost:8000/`.

Faites attention au port *8000*.

Vous devriez voir apparaitre le message "Hello world" dans le navigateur.

<<<

== Composer

La distribution dispose également de composer.

=== Présentation

Il s'agit d'un outil de gestion de dépendances pour PHP.

Il va vous permettre de définir quelles bibliothèques sont nécessaires à votre projet.

Composer gérera la récupération de ces bibliothèques (packages) et proposera un mécanisme d'autoloading.


=== Gestion des dépendances

==== Déclaration des dépendances

Créez un dossier pour accueillir votre premier projet.

À la racine du projet créez un fichier `composer.json`.
Nous allons définir une dépendance vers la bibliothèque `Monolog`. 
Pour cela ajoutez le code suivant.

.composer.json
[source,json]
----
{
    "require": { <1>
        "monolog/monolog": "1.*" <2>
    }
}
----
<1> bloc qui précise les packages à récupérer.
<2> la clé correspond au nom du package et à la valeur à la version souhaitée.

Le nom du package est décomposé en 2 parties, {nom de l'auteur (vendor)}/{nom du package}

La version peut être une version précise ou comme dans cet exemple la dernière version de la branche 1.x.


==== Dépendances pour la phase de développement

Certaines dépendances ne sont pas nécessaires au fonctionnement de l'application mais nécessaires à sa réalisation.
La bibliothèque `PHPUnit` qui permet de réaliser les tests unitaires et fonctionnels en est un bon exemple.

Ces dépendances sont déclarées à part.  Ceci permet de les exclure plus facilement  pour la livraison de l'application.

Voici comment ces dépendances sont déclarées.

.composer.json
[source,json]
----
...
    "require-dev": {
        "phpunit/phpunit": "^5.5"
    }
}
----

[NOTE]
====
Ne l'ajoutez pas pour l'instant nous en reparlerons quand nous aborderons PHPUnit.
====


=== Récupération des packages

La commande `install` va télécharger le package dans le dossier `vendor`.

Dans l'exemple suivant le package sera dans `vendor/monolog/monolog`.

Avant de lancer la commande ajoutez la section `autoload` au fichier `composer.json`.

[source, json]
.composer.json
----
    "autoload": {
        "psr-4": {
          "": "src/"
        }
    }
----

Lancez la commande depuis le terminal.

[source,json]
----
$ composer install
----

=== Chargement des dépendances

L'installation terminée le projet contient l'arborescence suivante.

[source,bash]
----
├── composer.json                                              
└── vendor
    ├── autoload.php
    ├── composer
    │   ├── autoload_classmap.php
    │   ├── autoload_namespaces.php
    │   ├── autoload_psr4.php
    │   ├── autoload_real.php
    │   ├── autoload_static.php
    │   ├── ClassLoader.php
    │   ├── installed.json
    │   └── LICENSE
    └── monolog
----

Le fichier `autoload.php` présent dans le dossier `vendor` prend en charge l'autoload des différents packages.

[NOTE]
====
Composer masque le système d'autoload sous-jacent de chaque package. 
Par exemple  PSR-0, PSR-4 ou encore namespace ... 

Nous reviendrons plus tard sur cette notion.
====

Nous ajoutons un fichier `index.php` à la racine du projet pour illustrer l'utilisation de l'autoload.

.index.php
[source,php]
----
<?php 

require __DIR__ . '/vendor/autoload.php'; <1>

$log = new Monolog\Logger('mon_application');
$log->pushHandler(new Monolog\Handler\StreamHandler('app.log', Monolog\Logger::WARNING));
$log->addWarning('Message d\'avertissement');
----
<1> Cette inclusion suffit à mettre en place le mécanisme d'autoload.

Utilisez la commande suivante pour lancer le serveur web interne.

[source, bash]
----
$ php -S localhost:8080
----

Ouvrez l'url dans votre navigateur. La page reste vide. 

Vérifiez la racine de votre projet vous noterez la présence d'un fichier `app.log`.


=== Créer une application avec composer

Créez un nouveau projet pour ce deuxième projet.

À la racine de votre projet lancez la commande `init` et répondez aux questions.

[source,bash]
----
$ composer init
                                            
  Welcome to the Composer config generator  
                                            
This command will guide you through creating your composer.json config.

Package name (<vendor>/<name>) [sylvain/application]: lpdw/mon-application
Description []: 
Author [Sylvain Floury <sylvain@floury.name>, n to skip]: 
Minimum Stability []: 
Package Type []: 
License []: 

Define your dependencies.

Would you like to define your dependencies (require) interactively [yes]? no <1>
Would you like to define your dev dependencies (require-dev) interactively [yes]? no

{
    "name": "lpdw/mon-application",
    "authors": [
        {
            "name": "Sylvain Floury",
            "email": "sylvain@floury.name"
        }
    ],
    "require": {}
}

Do you confirm generation [yes]? 
----
<1> Cette étape permet d'ajouter directement les dépendances externes.

Nous allons créer une application simple avec une classe que nous allons placer dans le dossier src/Lpdw/Hello.php.

[source, php]
.src/Lpdw/Hello.php
----
<?php
namespace Lpdw; // <1>

class Hello
{
    public function say($name) {
        echo "Hello " . $name;
    }
}
----
<1> namespace choisi pour notre application.

=== Autoloading

Nous ajoutons la section `autoload` au fichier `composer.json`.

[source, json]
.composer.json
----
    "autoload": {
        "psr-4": {
          "": "src/"
        }
    }
----

Lancez ensuite la commande `install` de composer qui va créer un dossier `vendor` avec un fichier `autoload.php`.

[source,bash]
----
├── composer.json
├── index.php
├── src
│   └── Lpdw
│       └── Hello.php
└── vendor
    ├── autoload.php
    └── composer
        ├── autoload_classmap.php
        ├── autoload_namespaces.php
        ├── autoload_psr4.php
        ├── autoload_real.php
        ├── autoload_static.php
        ├── ClassLoader.php
        ├── installed.json
        └── LICENSE
----

Nous allons ensuite créer un fichier `index.php`. L'arborescence de notre application est la suivante.

Dans le fichier `index.php` nous commençons par inclure le fichier `autoload.php` qui va prendre en charge l'autoload.

[source, php]
.index.php
----
<?php

require __DIR__."/vendor/autoload.php";

use Lpdw\Hello;

$hello = new Hello();
$hello->say("Sylvain"); 
----

Utilisez la commande suivante pour lancer le serveur web interne.

[source, bash]
----
$ php -S localhost:8080
----

Ouvrez l'url dans votre navigateur et vérifiez que le message apparaître.

==== PSR-4

La PSR-4 est une recommandation qui concerne le mécanisme d'autolaoding.
Cette recommandation est émise par le PHP-FIG (PHP Framework Interop Group).

Elle est disponible à l'adresse suivante : http://www.php-fig.org/psr/psr-4/
Différents exemples sont disponbiles.

Si nous revenons sur le fichier `autoload_psr4.php` nous voyons comment composer gère les bibliothèques une par une.

[source, php]
----
<?php

// autoload_psr4.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'Psr\\Log\\' => array($vendorDir . '/psr/log/Psr/Log'),
    'Monolog\\' => array($vendorDir . '/monolog/monolog/src/Monolog'),
);
----

=== Récupération de packages avec dépendances

Le package peut lui même avoir des dépendances dans ce cas elles seront également récupérées.

[source,bash]
----
$ composer.phar require symfony/http-foundation 
Using version ^3.1 for symfony/http-foundation
./composer.json has been updated
Loading composer repositories with package information
Updating dependencies (including require-dev)
  - Installing symfony/polyfill-mbstring (v1.2.0)
    Loading from cache

  - Installing symfony/http-foundation (v3.1.3)
    Loading from cache

Writing lock file
Generating autoload files
----

Vous remarquerez que seul `symfony/http-foundation` est présent dans le fichier `composer.json`.

[source,yaml]
----
"require": {
        "symfony/http-foundation": "^3.1"
    }
----

Comme nous n'avons pas précisé de version la dernière version est prise en compte ou plus exactement la version 3.1.* la plus récente.

Un autre fichier `composer.lock` contient les informations sur tous les packages installés qu'ils soient dans le fichier `composer.json` ou qu'il s'agisse d'une dépendance.

[NOTE]
====
Les commandes `install` et `update` vont se comporter différemment en présence du fichier `composer.lock`.

La commande `install` ne tient compte que du fichier `composer.lock` et permet de maîtriser la version des packages installés.
La commande `update` va d'abord mettre à jour le fichier `composer.lock` avant d'installer les packages.

Ceci permet de maîtriser dans une certaine mesure les versions des packages installés notamment ceux installés pour dépendance.
====

=== Commandes utiles

Liste toutes les bibliothèques présentes dans `vendor` y compris les dépendances. 

[source,bash]
----
$ composer show --installed
----

<<<

== PHP Unit

=== Création d'un projet de test

Créez un nouveau dossier pour accueillir ce projet.

[source, bash]
----
$ composer init


  Welcome to the Composer config generator



This command will guide you through creating your composer.json config.

Package name (<vendor>/<name>) [root/exemple_1_test_installation]: floury/outils_phpunit
Description []: Demonstration de l'utilisation de PHPUnit.
Author [Sylvain Floury <sylvain@floury.name>, n to skip]:
Minimum Stability []:
Package Type (e.g. library, project, metapackage, composer-plugin) []: project
License []:

Define your dependencies.

Would you like to define your dependencies (require) interactively [yes]? no
Would you like to define your dev dependencies (require-dev) interactively [yes]?
Search for a package: phpunit

Found 15 packages matching phpunit

   [0] phpunit/phpunit
   [1] jbzoo/phpunit
   [2] eher/phpunit
   [3] backplane/phpunit
   [4] ivan-chkv/yii2-phpunit
   [5] phpunit/phpunit-mock-objects
   [6] phpunit/phpunit-selenium
   [7] phpunit/phpunit-story
   [8] phpunit/phpunit-skeleton-generator
   [9] phpunit/dbunit
  [10] phpunit/phpunit-dom-assertions
  [11] phpunit/phpcov
  [12] phpunit/phpunit-mink-trait
  [13] phpunit/php-timer
  [14] phpunit/php-code-coverage

Enter package # to add, or the complete package name if it is not listed: 0
Enter the version constraint to require (or leave blank to use the latest version):
Using version ^5.4 for phpunit/phpunit
Search for a package:

{
    "name": "lpdw/outils_phpunit",
    "description": "Demonstration de l'utilisation de PHPUnit.",
    "type": "project",
    "require-dev": {
        "phpunit/phpunit": "^5.4"
    },
    "authors": [
        {
            "name": "Sylvain Floury",
            "email": "sylvain@floury.name"
        }
    ],
    "require": {}
}

Do you confirm generation [yes]?

$ composer install

$ vendor/bin/phpunit --version
----

Ajoutez le code suivant dans le fichier `composer.json`.

[source, json]
.composer.json
----
 "autoload": {
        "psr-4": {
          "": "src/"
        }
    }
----

Lancer de nouveau la commande composer install.

Ajouter le fichier source.

[source, php]
.src/Lpdw/StringUtils.php
----
<?php

namespace Lpdw;

class StringUtils
{
    public static function capitalize($string) {
        return strtoupper($string);
    }
}
----

Ajoutez le fichier de test suivant.

[source, php]
.test/Lpdw/StringUtilsTest.php
----
<?php

namespace Lpdw;

use PHPUnit\Framework\TestCase;

class StringUtilsTest extends TestCase
{
    public function testShouldCapitalizeString() {
        $this->assertEquals('TEST', StringUtils::capitalize('test'));
    }
}
----

Lancez la commande suivante.

[source, bash]
----
$ vendor/bin/phpunit --bootstrap vendor/autoload.php test/Lpdw/StringUtilsTest.php
----

=== Fichier de configuration

https://phpunit.de/manual/current/en/appendixes.configuration.html

==== Test suite

Il est possible de simplifier la réalisation des tests en utilisant des suites.

Fonction d'un dossier.

[source, xml]
----
<phpunit bootstrap="vendor/autoload.php">
  <testsuites>
    <testsuite name="lpdw">
      <directory>tests</directory>
    </testsuite>
  </testsuites>
</phpunit>
----

Fonction d'une liste définie de fichiers.

[source, xml]
----
<phpunit bootstrap="src/autoload.php">
  <testsuites>
    <testsuite name="lpdw">
      <file>tests/IntlFormatterTest.php</file>
      <file>tests/MoneyTest.php</file>
      <file>tests/CurrencyTest.php</file>
    </testsuite>
  </testsuites>
</phpunit>
----

Créez le fichier `phpunit.xml` à la racine du projet.

[source, xml]
.phpunit.xml
----
<phpunit bootstrap="vendor/autoload.php">
  <testsuites>
    <testsuite name="string-utils">
      <directory>test</directory>
    </testsuite>
  </testsuites>
</phpunit>
----

Lancez simplement la commande suivante pour réaliser le test.

[source, bash]
----
$ vendor/bin/phpunit
----

<<<

== IDE

PHP Storm est disponible sur la distribution.
Il s'agit d'une version d'évaluation mais vous pouvez obtenir une licence éducation d'un an sur le site de l'éditeur .footnote:[https://www.jetbrains.com/student/].

Nous allons importer le projet de test de phpunit dans l'IDE.

Pour cela commencez par lancer PHP Storm.

. Allez Dans le menu `File > New Project form ExistingFiles\...`.
. Choisissez ensuite le scenario sans serveur configuré (le dernier de la liste).
. Choisissez le repertoire qui correspond au projet.

Une fois le projet ouvert vous devriez retrouver tous les fichiers du projet.


=== Configuration de la CLI

Il nous faut configurer le projet avec la version de PHP que nous souhaitons utiliser.

. Appuyez sur les touches Ctrl + Alt + S (ou menu `File > Settings`).
. Une fois dans la fenêtre de dialogue `Settings` allez dans la rubrique `Languages & Frameworks > PHP`.
. Choisissez la version de PHP que vous souhaitez utiliser avec `PHP language level`. Dans notre cas nous choisirons la 7.1.
. Choisissez `/usr/bin/php` pour l'interpreteur. Vous devriez disposer de la version 7.1.9.


=== Création d'un premier test

Nous allons définir que le dossier `src` contient les sources et que le dossier `test` contient les tests.

. Dans la partie `Project` faites un clic droit sur l'un de ces dossiers.
. Dans l'entrée `Mark Directory as` choisisez `Sources Root` ou `Test Sources Root`.

Ensuite créez une nouvelle classe `SayHello`.

[source, php]
.src/Lpdw/SayHello.php
----
<?php

namespace Lpdw;

class SayHello
{
    public static function toSomeone($name) {
        return 'Hello ' + $name;
    }
}
----

Appuyez sur les touches Ctrl + Shift + T une boite de dialogue vous permet de générer simplement le test.

La classe de test créée complétez le test unitaire.

Pour lancer les tests faites un clic droit sur le dossier `test` et choisissez `Run test`.

[NOTE]
====
Pour lancer PHPUnit vous pouvez également passer par un terminal.

PHP Storm permet d'accéder à un terminal avec la combinaison de touches Alt + F12.
====

<<<

== Xdebug

La distribution fournie dispose de XDebug.
Ce debugger est un module PHP que nous pouvons activer ou désactiver.

Son utilisation peut perturber d'autres outils. Ainsi Composer recommande sa désactivation.

Pour vérifier son état nous pouvons passer par la CLI.
Utilisez la commande suivante :

[source,bash]
----
$ php -i | grep xdebug
----

Ceci retourne toutes les informations relatives à XDebug.

Celles qui nous intéressent sont suivantes:

[source,bash]
----
xdebug.remote_autostart => Off
xdebug.remote_enable => Off
----

Vous pouvez aussi vérifier  que le profiler est désactivé.

[source,bash]
----
xdebug.profiler_enable => Off
----

Pour désactiver complètement XDebug il suffit de commenter la déclaration de l'extension.
Pour cela préfixé la ligne suivante d'un "*;*".

[source,bash]
./etc/php.d/15-xdebug.ini
----
;zend_extension=xdebug.so
----

[NOTE]
====
Certaines distributions comme Debian fournissent des outils spécifiques à l'activation et désactivation des modules PHP.

Par exemple en tant que root la commande suivante désactive XDebug.

[source, bash]
----
# php5dismod xdebug
----

La commande suivante active XDebug.

[source, bash]
----
# php5enmod xdebug
----
====

=== Utilisation dans PHP Storm

Nous allons commencer par le configurer pour notre projet.

. Appuyez sur les touches Ctrl + Alt + S (ou menu `File > Settings`).
. Une fois dans la fenêtre de dialogue `Settings` allez dans la rubrique `Languages & Frameworks > PHP > Test Frameworks`.
. Ajoutez `PHPUnit Local`.
. Choisissez `Use Composer autoloader`.
. Selectionnez le fichier `vendor/autoload.php` dans `Path to script`.
. Enfin Selectionnez le fichier `phpunit.xml` pour `Test Runner > Default configuration file`.

Pour utiliser XDebug avec PHP Storm procédez de la façon suivante :

. Ouvrez le fichier `test/Lpdw/StringUtilsTest.php` et cliquez sur la ligne de l'assertion juste à droite du numéro de ligne.
. Ceci fait apparaitre un point rouge.
. Faites un clic droit et choisissez l'option `Debug`.

Le script s'exécute et s'arrête sur la ligne choisie.
Vous pouvez consulter les valeurs des différents éléments.

[NOTE]
====
Nous avons vu que XDebug n'est pas activé mais il est tout de même possible de préciser des options à l'exécution.

PHP Storm utilise ce système pour XDebug.
Voici un extrait de la commande exécutée.

[source, bash]
----
/usr/bin/php -dxdebug.remote_enable=1 -dxdebug.remote_mode=req -dxdebug.remote_port=9000 -dxdebug.remote_host=127.0.0.1
----
====